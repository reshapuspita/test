/* Author: Resha Puspita */

public class Main {


    public static void main(String[] args) {
        try{

            Rectangle r = new Rectangle(2,2,0,0);
            Rectangle r2 = (Rectangle)r.clone();

            System.out.println(r.x + "," + r.y);
            System.out.println(r2.x + "," + r2.y);
            System.out.println("H");
            System.out.println("I");
            
        } catch (CloneNotSupportedException c) {}
    }
}