public class Rectangle implements Cloneable {
    private Point origin;
    protected int weight;
    protected int height;
    protected int x;
    protected int y;
    public Rectangle(int weight, int height, int x, int y) {
        origin = new Point(x,y);
        this.x = x;
        this.y = y;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }
}
